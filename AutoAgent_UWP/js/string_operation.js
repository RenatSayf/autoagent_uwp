﻿function trimByFirstOccurrence(string, sub_string)
{
    var index = string.indexOf(sub_string);
    return string.slice(index);
}