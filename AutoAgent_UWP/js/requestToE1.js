﻿var pars = new DOMParser();
var docum;

var list_results = ".list_results";

function sendRequest()
{
    //$.post(queryBuilder(), function (data)
    //{
    //    var a = data;
    //    docum = pars.parseFromString(data, "text/html");
    //    var ul_tag = docum.getElementsByClassName("aum-offers-list _add_result");
    //    var doc1 = pars.parseFromString(ul_tag, "text/html");
    //    var li_tag = doc1.getElementsByClassName("aum-offers-list__item aum-offers-list__item_offers");
    //});


    var url = queryBuilder();
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);

    xhr.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate, sdch");
    xhr.setRequestHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    xhr.setRequestHeader("Cache-Control", "max-age=0");
    xhr.setRequestHeader("DNT", "1");
    xhr.setRequestHeader("Host", "m.auto.e1.ru");
    xhr.setRequestHeader("Upgrade-Insecure-Requests", "1");
    xhr.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");

    xhr.send();

    xhr.onreadystatechange = function ()
    {
        if (this.readyState != 4) return;

        if (this.status != 200)
        {
            new Windows.UI.Popups.MessageDialog("Ошибка " + this.status + " соединения с сервером").showAsync();
            Debug.write("Ошибка " + this.status + " соединения с сервером");
            return;
        } else if (this.status === 200)
        {
            Debug.write("Cоединение с сервером установлено. Статус - " + this.statusText);
        }
    }

    xhr.onload = function ()
    {
        $('.offers-list_item').remove();

        docum = pars.parseFromString(this.responseText, "text/html");
        var ul_tag = docum.getElementsByClassName("aum-offers-list _add_result");
        
        if (ul_tag.length > 0)
        {
            var doc1 = pars.parseFromString(ul_tag['0'].innerHTML, "text/html")

            var a_item_link = doc1.getElementsByClassName("aum-offers-list__item-link _item_link");
            var span_item_date = doc1.getElementsByClassName("aum-offers-list__item-date");
            var span_item_name = doc1.getElementsByClassName("aum-offers-list__item-name");
            var img_item_img = doc1.getElementsByClassName("aum-offers-list__item-img");
            var span_item_price = doc1.getElementsByClassName("aum-offers-list__item-price");
            var span_item_tech = doc1.getElementsByClassName("aum-offers-list__item-tech");
            var span_item_location = doc1.getElementsByClassName("aum-offers-list__item-location");
            
            try
            {
                var i = 0;
                while (a_item_link[i] != null)
                {
                    $(list_results).append('<li class="offers-list_item">' +
                                                '<a class="offers-list__item-link" href="' + a_item_link[i].href + '">' +

                                                    '<div>' +

                                                        '<span>' + span_item_date[i].innerText + '</span>' +
                                                        '<span>   ' + span_item_name[i].innerText + '</span>' +

                                                    '</div>' +

                                                    '<figure >' +

                                                        '<p>' +
                                                            '<img src="' + urlE1.base + trimByFirstOccurrence(img_item_img[i].src, "pview") + '">' +
                                                        '</p>' +

                                                        '<figcaption>' +

                                                            '<div>' +

                                                                '<span>' + span_item_price[i].innerText + '</span>' +

                                                            '</div>' +

                                                            '<div>' +

                                                                '<span>' + span_item_tech[i].innerText + '</span><br/>' +
                                                                '<span>' + span_item_location[i].innerText + '</span>' +

                                                            '</div>' +

                                                        '</figcaption>' +

                                                    '</figure>' +

                                                '</a>' +
                                           '</li>');
                    i++;
                }
            } catch (e)
            {
                Debug.write("Возникло исключение - " + e.description);
            }
        }
        else
        {
            $(list_results).append('<li class="offers-list_item">' +
                                        '<span>Ничего не найдено</span>' + 
                                    '</li>');
        }
    }
}























