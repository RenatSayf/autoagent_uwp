﻿(function() 
{
  'use strict';
  var app = WinJS.Application;
  var activation = Windows.ApplicationModel.Activation;

  app.onactivated = function (args) 
  {
    if (args.detail.kind === activation.ActivationKind.launch) 
    {
      if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) 
      {
          // TODO: это приложение только что запущено. Инициализируйте здесь свое приложение.
          initHtml();
          initAvito();
          
          selectTagRegion_OnChange();
          pivot_SelectionChanged();
          select_OnChange();
          checkBox_onClick();
          button_Click();
          
      } else 
      {
        // TODO: это приложение было активировано из приостановленного состояния.
        // Место для состояния восстановления приложения.
      }
      args.setPromise(WinJS.UI.processAll().then(function() 
      {
        // TODO: место для кода.
      }));
    }
  };
  app.oncheckpoint = function (args)
  {
      var a = args;
      // TODO: действие приложения будет приостановлено. Сохраните здесь все состояния, которые понадобятся после приостановки.
      // Вы можете использовать объект WinJS.Application.sessionState, который автоматически сохраняется и восстанавливается после приостановки.
      // Если вам нужно завершить асинхронную операцию до того, как действие приложения будет приостановлено, вызовите args.setPromise().
      Debug.write("Сработал app.oncheckpoint - " + args);
  };

    



  app.start();
}());
//***************************************************************************************************************************************************
var selectTagSite;
var selectTagRegion;
var selectTagCategory;
var selectTagMark;
var selectTagModel;
var pivot;
var split_view;
var selectBodyType;
var selectTagEngineType;
var checkbox_wheel_left;

var mark_id = '#mark';
var model_id = '#model';
var price_min_id = '#price_min';
var price_max_id = '#price_max';

var urlE1 = new Object();

// тело запроса
urlE1.base = "http://m.auto.e1.ru/";
urlE1.search = "search/";
urlE1.category = "car/";
urlE1.used = "";
urlE1.new = "";
urlE1.mark = "";
urlE1.model = "";

//параметры запроса
urlE1.price_min = "";               //price[min]=
urlE1.price_max = "";               //price[max]=
urlE1.price_currency = "price_currency=rur";
urlE1.year_min = "";                //year[min]
urlE1.year_max = "";                //year[max]
urlE1.region = "region[]=213";      //region[]=
urlE1.with_photo = "";              //with_photo=1
urlE1.not_damaged = "pure=0";             //pure=1
urlE1.rubric16 = "";                //rubric[]=16
urlE1.rubric15 = "";                //rubric[]=15
urlE1.private_person = "";          //seller[]=1
urlE1.car_showroom = "";            //seller[]=3
urlE1.official_dealers = "";        //seller[]=4
urlE1.run_size_min = "";            //run_size[min]=1
urlE1.run_size_max = "";            //run_size[max]=200
urlE1.wheel_left = "";              //wheel[]=128
urlE1.wheel_right = "";             //wheel[]=129
urlE1.transmission_auto = "";       //transmission[]=130
urlE1.transmission_main = "";       //transmission[]=131
urlE1.transmission_var = "";        //transmission[]=132
urlE1.transmission_robot = "";      //transmission[]=133
urlE1.front = "";                   //gear[]=125
urlE1.back = "";                    //gear[]=126
urlE1.full = "";                    //gear[]=127
urlE1.body_type = "";               //type[]=45
urlE1.engine_type = "";             //engine_type[]=24559
urlE1.horsepower_min = "";          //horsepower[min]=100
urlE1.horsepower_max = "";          //horsepower[max]=500
urlE1.capacity_humans = "";         //capacity_humans=4-5
urlE1.body_color = "";              //body_color[]=23
urlE1.status_in_stock = "";         //auto_status[]=151
urlE1.status_another_city = "";     //auto_status[]=153
urlE1.status_on_way = "";           //auto_status[]=152
urlE1.broken = "";                  //condition[]=42
urlE1.for_details = "";             //condition[]=43
urlE1.exchange = "";                //extra[]=1
urlE1.credit = "";                  //extra[]=2
urlE1.trade_in = "";
urlE1.more_cheaper = "";            //exchange_ext[]=31450
urlE1.more_expensive = "";          //exchange_ext[]=31451
urlE1.equal_value = "";             //exchange_ext[]=31452
urlE1.not_cars = "";                //exchange_ext[]=31453
urlE1.period = "";                  //period=three_days


//===================================================================================================================================================
function initHtml()
{
    pivot = new WinJS.UI.Pivot(document.getElementById("pivot"));
    split_view = new WinJS.UI.SplitView(document.getElementById("split_view"));
    selectTagSite = document.getElementById("site");
    selectTagRegion = document.getElementById("region");
    selectTagCategory = document.getElementById("category");
    selectTagMark = document.getElementById("mark");
    selectTagModel = document.getElementById("model");
    selectBodyType = document.getElementById("body_type");
    selectTagEngineType = document.getElementById("engine_type");
    
    document.getElementById('price_min').onkeypress = function (e)
    {
        return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode))); 
    }

    document.getElementById('price_max').onkeypress = function (e)
    {
        return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
    }

    document.getElementById('run_size_min').onkeypress = function (e)
    {
        return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
    }

    document.getElementById('run_size_max').onkeypress = function (e)
    {
        return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
    }

    document.getElementById('horsepower_min').onkeypress = function (e)
    {
        return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
    }

    document.getElementById('horsepower_max').onkeypress = function (e)
    {
        return !(/[А-Яа-яA-Za-z ]/.test(String.fromCharCode(e.charCode)));
    }

    setListYearsToSelect(year_min);
    setListYearsToSelect(year_max);
    
}
//===================================================================================================================================================
var count = 0;
function pivot_SelectionChanged()
{
    $('#pivot').bind('selectionchanged', function ()
    {
        if (pivot.selectedIndex === 1 && count < 1)
        {
            var url = urlE1.base + urlE1.search + urlE1.category;
            region_change = true;
            category_change = true;
            mark_change = true;
            model_change = true; 
            getDomModelE1(url);
            count++;
        }
        else if (pivot.selectedIndex === 2)
        {
            getDomModelAvito();
        }
    });
}
//===================================================================================================================================================
function selectTagRegion_OnChange()
{
    //selectTagRegion.addEventListener("change", function ()
    //{
    //    urlE1.region = "region[]=" + selectTagRegion.value;
    //    var url = urlE1.search + urlE1.category + "?";
    //    var params = urlE1.region;
    //    getDomModelE1(url, params);
    //}
    //, false);
}
//===================================================================================================================================================
var xhr = new XMLHttpRequest();
var region_change = false;
var category_change = false;
var mark_change = false;
var model_change = false;

function getDomModelE1(url, params)
{
    
    params = "";
    xhr.open('POST', url, true);

    xhr.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate, sdch");
    xhr.setRequestHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
    xhr.setRequestHeader("Cache-Control", "max-age=0");
    xhr.setRequestHeader("DNT", "1");
    xhr.setRequestHeader("Host", "m.auto.e1.ru");
    xhr.setRequestHeader("Upgrade-Insecure-Requests", "1");
    xhr.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36");
    
    xhr.send();

    xhr.onreadystatechange = function ()
    {
        if (this.readyState != 4) return;

        if (this.status != 200)
        {
            new Windows.UI.Popups.MessageDialog("Ошибка " + this.status + " соединения с сервером").showAsync();
            Debug.write("Ошибка " + this.status + " соединения с сервером");
            return;
        } else if (this.status === 200)
        {
            Debug.write("Cоединение с сервером установлено. Статус - " + this.statusText); 
        }
    }
    
    xhr.onload = function ()
    {
        if (region_change)
        {
            setContentToSelectRegion(this.responseText);
            setContentToSelectCategory(this.responseText);
            setContentToSelectMark(this.responseText);
            setContentToSelectCapacityHumans(this.responseText);
            setContentToSelectBodyColor(this.responseText);
            setContentToSelectPeriod(this.responseText);
        }
        if (category_change)
        {
            setContentToSelectCategory(this.responseText);
            setContentToSelectMark(this.responseText);
            setContentToSelectModel(this.responseText);
            setContentToSelectCapacityHumans(this.responseText);
            setContentToSelectBodyColor(this.responseText);
            setContentToSelectPeriod(this.responseText);
        }
        if (mark_change)
        {
            setContentToSelectMark(this.responseText);
            setContentToSelectBodyType(this.responseText);
            setContentToSelectEngineType(this.responseText);
        }
        if (model_change)
        {
            setContentToSelectModel(this.responseText);
        }

        region_change = false;
        category_change = false;
        mark_change = false;
        model_change = false;
    }
}
//===================================================================================================================================================
function getDOMParser(textParse)
{
    var pars = new DOMParser();
    var document = pars.parseFromString(textParse, "text/html");
    return document;
}
//===================================================================================================================================================
function setContentToSelectRegion(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");
    
    
    var regionsTag = docum.getElementsByName("region[]");
        
    try
    {
        if (regionsTag.length > 0 && regionsTag['region[]'][0] != null)
        {
            selectTagRegion.length = 0;
            var i = 0;
            while (regionsTag['region[]'][i] != null)
            {
                selectTagRegion.options[i] = new Option(regionsTag['region[]'][i].innerText, "");
                selectTagRegion.options[i].value = regionsTag['region[]'][i].value;
                selectTagRegion.options[i].selected = regionsTag['region[]'][i].selected;
                i++;
            }
        }
    } catch (e)
    {
        //Debug.write("homme.js setContentToSelectRegion(textParse) TagName region[] не найден.   region[] = " + regionsTag + ",   regionsTag.length = " + regionsTag.length);
        Debug.write("Исключение - " + e.description);
        selectTagRegion.options[0] = new Option("Нет данных", null);
        return false;
    }
    return true;
}
//===================================================================================================================================================
function setContentToSelectCategory(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");

    var categoryTag = docum.getElementsByName("category");

    try
    {
        if (categoryTag.length > 0 && categoryTag['category'][0] != null)
        {
            $('#' + category.id).empty();
            var i = 0;
            while (categoryTag['category'][i] != null)
            {
                var attribute = categoryTag['category'][i].getAttribute("data-name");
                $('#' + category.id).append($('<option ' + 'data-name="' + attribute + '">' + categoryTag['category'][i].innerText + '</option>'))
                i++;
            }
        }
    } catch (e)
    {
            Debug.write("Исключение - " + e.description);
            return false;
    }
    return true;
}
//===================================================================================================================================================
function setContentToSelectMark(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");
        
    var markTag = docum.getElementsByName("mark");

    try
    {
        
        if (markTag.length > 0 && markTag['mark'][0] != null)
        {
            //selectTagMark.length = 0;
            $('#' + mark.id).empty();
            var i = 0;
            while (markTag['mark'][i] != null)
            {
                selectTagMark.options[i] = new Option(markTag['mark'][i].innerText, "");
                selectTagMark.options[i].value = markTag['mark'][i].getAttribute("data-name");
                i++;
            }
        }
    } catch (e)
    {
        Debug.write("Исключение - " + e.description);
        return false;
    }
    return true;
}
//===================================================================================================================================================
function setContentToSelectModel(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");

    var tag_name = 'model';
    var modelTag = docum.getElementsByName(tag_name);

    try
    {
        //selectTagModel.length = 0;
        if (modelTag.length > 0 && modelTag[tag_name][0] != null)
        {
            $('#' + model.id).empty();
            var i = 0;
            while (modelTag[tag_name][i] != null)
            {
                //selectTagModel.options[i] = new Option(modelTag['model'][i].innerText, "");

                var attribute = modelTag[tag_name][i].getAttribute("data-name");
                $('#' + model.id).append($('<option ' + 'data-name="' + attribute + '">' + modelTag[tag_name][i].innerText + '</option>'))
                i++;
            }
        }
        if (modelTag.length === 0)
        {
            $('#' + model.id).empty();
            $('#' + model.id).append($('<option ' + 'selected>' + "Все модели" + '</option>'));
        }
    } catch (e)
    {
        Debug.write("Исключение - " + e.description);
        return false;
    }
    return true;
}
//===================================================================================================================================================
function setContentToSelectBodyType(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");
    
    var tag_name = 'type[]';
    var typeTag = docum.getElementsByName(tag_name);

    try
    {
        if (typeTag.length > 0 && typeTag[tag_name][0] != null)
        {
            $('#' + body_type.id).empty();
            $('#' + body_type.id).append($('<option selected>Не важно</option>'));
            var i = 0;
            while (typeTag[tag_name][i] != null)
            {
                $('#' + body_type.id).append($('<option value="' + typeTag[tag_name][i].value + '">' + typeTag[tag_name][i].innerText + '</option>'));
                i++;
            }
        }
    } catch (e)
    {
        Debug.write("Исключение - " + e.description);
        return false;
    }
    return true;
}
//===================================================================================================================================================
function setContentToSelectEngineType(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");
    var tag_name = 'engine_type[]';
    var engineTag = docum.getElementsByName(tag_name);
    try
    {
        if (engineTag.length > 0 && engineTag[tag_name][0] != null)
        {
            $('#' + engine_type.id).empty();
            $('#' + engine_type.id).append($('<option selected>Не важно</option>'));
            var i = 0;
            while (engineTag[tag_name][i] != null)
            {
                $('#' + engine_type.id).append($('<option value="' + engineTag[tag_name][i].value + '">' + engineTag[tag_name][i].innerText + '</option>'));
                i++;
            }
            
        }
    } catch (e) 
    {
        Debug.write("Исключение - " + e.description);
        return false;
    }
    return true;
}
//===================================================================================================================================================
function setContentToSelectCapacityHumans(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");
    var tag_name = 'capacity_humans';
    var capacity_tag = docum.getElementsByName(tag_name);
    try
    {
        if(capacity_tag.length > 0 && capacity_tag[tag_name][0] != null)
        {
            document.getElementById(capacity_humans.id).length = 0;
            var i = 0;
            while (capacity_tag[tag_name][i] != null)
            {
                $(capacity_humans).append($('<option value="' + capacity_tag[tag_name][i].value + '">' + capacity_tag[tag_name][i].innerText + '</option>'));
                i++;
            }
        }
    } catch (e)
    {
        Debug.write("Исключение - " + e.description);
        return false;
    }

    return true;
}
//===================================================================================================================================================
function setContentToSelectBodyColor(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");
    var tag_name = 'body_color[]';
    var body_color_tag = docum.getElementsByName(tag_name);
    try
    {
        if (body_color_tag.length > 0 && body_color_tag[tag_name][0] != null)
        {
            $('#' + body_color.id).empty();
            var i = 0;
            while (body_color_tag[tag_name][i] != null)
            {
                $('#' + body_color.id).append($('<option value="' + body_color_tag[tag_name][i].value + '">' + body_color_tag[tag_name][i].innerText + '</option>'))
                i++;
            }
        }
    } catch (e)
    {
        Debug.write("Исключение - " + e.description);
        return false;
    }

    return true;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------
function setContentToSelectPeriod(textParse)
{
    var pars = new DOMParser();
    var docum = pars.parseFromString(textParse, "text/html");
    var tag_name = 'period';
    var period_tag = docum.getElementsByName(tag_name);
    try
    {
        if (period_tag.length > 0 && period_tag[tag_name][0] != null)
        {
            $('#' + period.id).empty();
            var i = 0;
            while (period_tag[tag_name][i] != null)
            {
                $('#' + period.id).append($('<option value="' + period_tag[tag_name][i].value + '">' + period_tag[tag_name][i].innerText + '</option>'))
                i++;
            }
        }
    } catch (e)
    {
        Debug.write("Исключение - " + e.description);
        return false;
    }

    return true;
}
//===================================================================================================================================================
function select_OnChange()
{
    var event_name = 'change';
    $('#' + region.id).bind(event_name, function ()
    {
        urlE1.region = "region[]=" + this.value;
        var url = urlE1.base + urlE1.search + urlE1.category + "?" + urlE1.region;
        var params = urlE1.region;
        category_change = true;
        mark_change = true;
        model_change = true;
        getDomModelE1(url, params);
    });
    //---------------------------------------------------------------------------------------------
    $('#' + mark.id).bind(event_name, function (eventObj)
    {
        urlE1.mark = $('#' + mark.id).val() + "/";
        var url = urlE1.base + urlE1.search + urlE1.category + urlE1.mark + "?" + urlE1.region;
        var params = urlE1.region;
        model_change = true;
        getDomModelE1(url, params);
    });
    //---------------------------------------------------------------------------------------------
    $('#'+category.id).bind(event_name, function ()
    {
        urlE1.search = "search/";
        urlE1.category = "car/";
        urlE1.category = $('#'+category.id +' option:selected').attr("data-name") + "/";
        if (urlE1.category === "car/")
        {
            $(div_body_engine).show();
        } else
        {
            $(div_body_engine).hide();
        }
        var url = urlE1.base + urlE1.search + urlE1.category + "?" + urlE1.region + "&" + urlE1.not_damaged;
        var params = urlE1.region;
        mark_change = true;
        model_change = true;
        getDomModelE1(url, params);
    });
    //---------------------------------------------------------------------------------------------
    $('#' + model.id).bind(event_name, function (eventObj)
    {
        urlE1.model = "";
        urlE1.model = $('#' + model.id + ' option:selected').attr("data-name") + "/";
    });
    //---------------------------------------------------------------------------------------------
    $('#' + price_min.id).bind(event_name, function (eventObj)
    {
        urlE1.price_min = "";
        if ($('#' + price_max.id).val() != "" && !checkDifference($('#' + price_min.id).val(), $('#' + price_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильное значение").showAsync();
            $('#' + price_min.id).val("");
            return;
        }
        urlE1.price_min = "price[min]=" + encodeURIComponent(this.value);
    });
    //---------------------------------------------------------------------------------------------
    $('#' + price_max.id).bind(event_name, function (eventObj)
    {
        urlE1.price_max = "";
        if ($('#' + price_min.id).val() != "" && !checkDifference($('#' + price_min.id).val(), $('#' + price_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильное значение").showAsync();
            $('#' + price_max.id).val("");
            return;
        }
        urlE1.price_max = "price[max]=" + encodeURIComponent(this.value);
    });
    //---------------------------------------------------------------------------------------------
    $(price_currency).bind(event_name, function (eventObj)
    {
        Windows.UI.Popups.MessageDialog(this.value).showAsync();
    });
    //---------------------------------------------------------------------------------------------
    $('#' + body_type.id).bind(event_name, function (eventObj)
    {
        urlE1.body_type = "";
        urlE1.body_type = "type[]=" + $('#' + body_type.id + ' option:selected').val();
    });
    //---------------------------------------------------------------------------------------------
    $('#' + engine_type.id).bind(event_name, function (eventObj)
    {
        urlE1.engine_type = "";
        urlE1.engine_type = "engine_type[]=" + $('#' + engine_type.id + ' option:selected').val();
    });
    //---------------------------------------------------------------------------------------------
    $('#' + run_size_min.id).bind(event_name, function (eventObj)
    {
        urlE1.run_size_min = "";
        if ($('#' + run_size_max.id).val() != "" && !checkDifference($('#' + run_size_min.id).val(), $('#' + run_size_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильное значение").showAsync();
            $('#' + run_size_min.id).val("");
            return;
        }
        urlE1.run_size_min = "run_size[min]=" + this.value;
    });
    //---------------------------------------------------------------------------------------------
    $('#' + run_size_max.id).bind(event_name, function (eventObj)
    {
        urlE1.run_size_max = "";
        if ($('#' + run_size_min.id).val() != "" && !checkDifference($('#' + run_size_min.id).val(), $('#' + run_size_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильное значение").showAsync();
            $('#' + run_size_max.id).val("");
            return;
        }
        urlE1.run_size_max = "run_size[max]=" + this.value;
    });
    //---------------------------------------------------------------------------------------------
    $('#' + year_min.id).bind(event_name, function ()
    {
        urlE1.year_min = "";
        if ($('#' + year_max.id).val() != "" && !checkDifference($('#' + year_min.id).val(), $('#' + year_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильная дата").showAsync();
            $('#' + year_min.id).val("");
            return;
        }
        urlE1.year_min = "year[min]=" + this.value;
    });
    //---------------------------------------------------------------------------------------------
    $('#' + year_max.id).bind(event_name, function ()
    {
        urlE1.year_max = "";
        if ($('#' + year_min.id).val() != "" && !checkDifference($('#' + year_min.id).val(), $('#' + year_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильная дата").showAsync();
            $('#' + year_max.id).val("");
            return;
        }
        urlE1.year_max = "year[max]=" + this.value;
    });

    //---------------------------------------------------------------------------------------------
    $('#'+horsepower_min.id).bind(event_name, function ()
    {
        urlE1.horsepower_min = "";
        if ($('#' + horsepower_max.id).val() != "" && !checkDifference($('#' + horsepower_min.id).val(), $('#' + horsepower_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильное значение").showAsync();
            $('#' + horsepower_min.id).val("");
            return;
        }
        urlE1.horsepower_min = "horsepower[min]=" + this.value;
    });
    //---------------------------------------------------------------------------------------------
    $('#' + horsepower_max.id).bind(event_name, function ()
    {
        urlE1.horsepower_max = "";
        if ($('#' + horsepower_min.id).val() != "" && !checkDifference($('#' + horsepower_min.id).val(), $('#' + horsepower_max.id).val()))
        {
            Windows.UI.Popups.MessageDialog("Неправильное значение").showAsync();
            $('#' + horsepower_max.id).val("");
            return;
        }
        urlE1.horsepower_max = "horsepower[max]=" + this.value;
    });
    //---------------------------------------------------------------------------------------------
    $('#' + capacity_humans.id).bind(event_name, function ()
    {
        urlE1.capacity_humans = "";
        urlE1.capacity_humans = "capacity_humans=" + $('#' + capacity_humans.id + ' option:selected').val();
    });
    //---------------------------------------------------------------------------------------------
    $('#' + body_color.id).bind(event_name, function ()
    {
        urlE1.body_color = "";
        urlE1.body_color = "body_color[]=" + $('#' + body_color.id + ' option:selected').val();
    });
    //---------------------------------------------------------------------------------------------
    $('#' + period.id).bind(event_name, function ()
    {
        urlE1.period = "";
        urlE1.period = "period=" + $('#' + period.id + ' option:selected').val();
    });
    
}
//===================================================================================================================================================
function checkBox_onClick()
{
    var event_name = 'click';
    $('#' + with_photo.id).bind(event_name, function ()
    {
        if (!$('#' + with_photo.id).prop('checked'))
        {
            urlE1.with_photo = "";
        }
        else
        {
            urlE1.with_photo = "with_photo=1";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + not_damaged.id).bind(event_name, function ()
    {
        if (!$('#' + not_damaged.id).prop('checked'))
        {
            urlE1.not_damaged = "";
        }
        else
        {
            urlE1.not_damaged = "pure=1";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + used.id).bind(event_name, function ()
    {
        if (!$('#' + used.id).prop('checked'))
        {
            urlE1.used = "";
            urlE1.rubric16 = "";
        }
        else
        {
            urlE1.used = "used/";
            urlE1.rubric16 = "rubric[]=16";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + new_car.id).bind(event_name, function ()
    {
        if (!$('#' + new_car.id).prop('checked'))
        {
            urlE1.new = "";
            urlE1.rubric15 = "";
        }
        else
        {
            urlE1.new = "new/"; 
            urlE1.rubric15 = "rubric[]=15";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + private_person.id).bind(event_name, function ()
    {
        if (!$('#' + private_person.id).prop('checked'))
        {
            urlE1.private_person = ""; 
        }
        else
        {
            urlE1.private_person = "seller[]=1";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + car_showroom.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.car_showroom = "";
        }
        else
        {
            urlE1.car_showroom = "seller[]=3";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + official_dealers.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.official_dealers = "";
        }
        else
        {
            urlE1.official_dealers = "seller[]=4";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + wheel_left.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.wheel_left = "";
        }
        else
        {
            urlE1.wheel_left = "wheel[]=128";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + wheel_right.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.wheel_right = "";
        }
        else
        {
            urlE1.wheel_right = "wheel[]=129";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + transmission_auto.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.transmission_auto = "";
        }
        else
        {
            urlE1.transmission_auto = "transmission[]=130";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + transmission_main.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.transmission_main = "";
        }
        else
        {
            urlE1.transmission_main = "transmission[]=131";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + transmission_robot.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.transmission_robot = "";
        }
        else
        {
            urlE1.transmission_robot = "transmission[]=133";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + transmission_var.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.transmission_var = "";
        }
        else
        {
            urlE1.transmission_var = "transmission[]=132";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + front.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.front = "";
        }
        else
        {
            urlE1.front = "gear[]=125";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + back.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.back = "";
        }
        else
        {
            urlE1.back = "gear[]=126";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + full.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.full = "";
        }
        else
        {
            urlE1.full = "gear[]=127";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + status_in_stock.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.status_in_stock = "";
        }
        else
        {
            urlE1.status_in_stock = "auto_status[]=151";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + status_another_city.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.status_another_city = "";
        }
        else
        {
            urlE1.status_another_city = "auto_status[]=153";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + status_on_way.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.status_on_way = "";
        }
        else
        {
            urlE1.status_on_way = "auto_status[]=152";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + broken.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.broken = "";
            
        }
        else
        {
            urlE1.broken = "condition[]=42";
            urlE1.not_damaged = "";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + for_details.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.for_details = "";
        }
        else
        {
            urlE1.for_details = "condition[]=43";
            urlE1.not_damaged = "";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + exchange.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.exchange = "";
            $('#' + terms.id).hide();
        }
        else
        {
            urlE1.exchange = "extra[]=1";
            $('#' + terms.id).show();
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + credit.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.credit = "";
        }
        else
        {
            urlE1.credit = "extra[]=2";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + trade_in.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.trade_in = "";
        }
        else
        {
            urlE1.trade_in = "";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + more_cheaper.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.more_cheaper = "";
        }
        else
        {
            urlE1.more_cheaper = "exchange_ext[]=31450";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + more_expensive.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.more_expensive = "";
        }
        else
        {
            urlE1.more_expensive = "exchange_ext[]=31451";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + equal_value.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.equal_value = "";
        }
        else
        {
            urlE1.equal_value = "exchange_ext[]=31452";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + not_cars.id).bind(event_name, function ()
    {
        if (!this.checked)
        {
            urlE1.not_cars = "";
        }
        else
        {
            urlE1.not_cars = "exchange_ext[]=31453";
        }
    });
    //---------------------------------------------------------------------------------------------
    $('#' + btn_show.id).bind(event_name, function ()
    {
        queryBuilder();
        //window.location.href = "Pages/E1/resSearchE1.html";
        $('#' + search_e1.id).hide();
        $('#' + result_search_e1.id).show();
        sendRequest();
    });
    //---------------------------------------------------------------------------------------------
    $('#' + btn_back.id).bind(event_name, function ()
    {
        $('#' + result_search_e1.id).hide();
        $('#' + search_e1.id).show();
    });
}
//===================================================================================================================================================
function setListYearsToSelect(id)
{
    var date_now = new Date();
    var year_now=date_now.getFullYear();
    $(id).append($('<option selected>Не важно</option>'))
    for (var i = year_now; i >= 1900; i--)
    {
        $(id).append($('<option>' + i + '</option>'));
    }
    
}
//===================================================================================================================================================
function checkDifference(min, max)
{
    return max - min >= 0 ? true : false;
}
//===================================================================================================================================================
function queryBuilder()
{
    urlE1.search = "";
    var query = "";
    var query_body = "";

    for (var key in urlE1)
    {
        query_body += urlE1[key];
        if (key === "model")
        {
            break;
        }
    }
    query_body += "?";

    var query_params = "";
    var str = "";
    for (var key in urlE1)
    {
        if (key === "base" || key === "search" || key === "category" || key === "used" || key === "new" || key === "mark" || key === "model")
        {
            continue;
        }
        if (urlE1[key] != "")
        {
            str += "&" + urlE1[key];
        }
    }
    query_params = str.substring(1);
    query = query_body + query_params;
    return query;
}
//===================================================================================================================================================
function queryReset()
{
    for (var key in urlE1)
    {
        urlE1[key] = "";
    }
    urlE1.base = "http://m.auto.e1.ru/";
    urlE1.search = "search/";
    urlE1.category = "car/";
    urlE1.price_currency = "price_currency=rur";
    urlE1.region = "region[]=213";     
    urlE1.not_damaged = "pure=0";
}








